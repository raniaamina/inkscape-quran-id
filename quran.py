#! /usr/bin/env python
from alquran_id import AlQuran as Quran
import inkex
from inkex.elements import TextElement, Tspan

class quranID(inkex.GenerateExtension):

    def add_arguments(self, pars):
        pars.add_argument("--surat", type=int, default=1, help="Surat ke berapa dari 114 surat dalam Al-Quran")
        pars.add_argument("--ayat-start", type=int, default=1, help="Start Ayat ke berapa dari surat yang dipilih")
        pars.add_argument("--ayat-end", type=int, default=1, help="End Ayat ke berapa dari surat yang dipilih")
        pars.add_argument("--generate-opt", type=str, default="ayat", help="Pilihan yang akan digenerate")
        pars.add_argument("--font-quran", type=str, default="Al Qalam Quran Majeed IQI", help="Font untuk ayat Quran")
        pars.add_argument("--font-size", type=float, default=7, help="Ukuran Font untuk ayat Quran")
        pars.add_argument("--space", type=float, default=1.5, help="Spasi antar baris")
        pars.add_argument("--inline-size", type=int, default=150, help="Lebar maksimal untuk text di warp")
        pars.add_argument("--color", type=inkex.Color, default="#000000", help='Warna untuk Ayat Quran')
        pars.add_argument("--align", type=str, default="center", help="Format text")
        pars.add_argument("--tab", help="Tab")

    def Ayat(self):
        quran = Quran()
        bismillah = u"\uFDFD" + "\n"
        ayat = [bismillah]
        
        if self.options.ayat_start == self.options.ayat_end:
            ayt = quran.Ayat(self.options.surat, self.options.ayat_start)
            arn = quran.ArNumber(self.options.ayat_start)
            ayat.append(ayt)
            ayat = "".join(ayat)

        else:
            for a in range(self.options.ayat_start, self.options.ayat_end + 1):
                ayt = quran.Ayat(self.options.surat, a)
                arn = quran.ArNumber(a)
                arn = u"\u06DD" + arn 
                ayt = f" {ayt}  {arn} "
                ayat.append(ayt)
            ayat = "".join(ayat)
        return ayat

    def Terjemahan(self):
        quran = Quran()
        terjemahan = []
        nama_surat = quran.Surat(self.options.surat)
        
        if self.options.ayat_start == self.options.ayat_end:
            tjm = quran.Terjemahan(self.options.surat, self.options.ayat_start)
            tjm = f"{tjm}"
            terjemahan.append(tjm)
            nama_surat = f"\n (QS. {nama_surat[1]} ayat {self.options.ayat_start})"
            terjemahan.append(nama_surat)
            terjemahan = "\n".join(terjemahan)
        else:
            for t in range(self.options.ayat_start, self.options.ayat_end + 1):
                tjm = quran.Terjemahan(self.options.surat, t)
                tjm = f"  {t}). {tjm}"
                terjemahan.append(tjm)
            nama_surat = f"\n (QS. {nama_surat[1]} ayat {self.options.ayat_start}:{self.options.ayat_end})"
            terjemahan.append(nama_surat)
            terjemahan = "\n".join(terjemahan)
        return terjemahan

    def generate(self):
        quran_generate = TextElement()
        style_ayat = f"inline-size: {self.options.inline_size}; line-height: {self.options.space}; font-family: {self.options.font_quran}; font-size: {self.options.font_size}; text-align: {self.options.align}; white-space: pre; fill: {self.options.color}; direction: rtl;"
        style_terjemahan = f"inline-size: {self.options.inline_size}; line-height: {self.options.space}; font-family: serif; fill: #000000; font-size: {self.options.font_size}; text-align: {self.options.align}; white-space: pre; fill: {self.options.color};"

        if self.options.generate_opt == "ayat":
            quran_generate.style = style_ayat 
            quran_generate.text = self.Ayat()
        elif self.options.generate_opt == "terjemahan":
            quran_generate.style = style_terjemahan 
            quran_generate.text = self.Terjemahan()
        elif self.options.generate_opt == "ayat dan terjemahan":
            quran_generate.style = style_ayat 
            quran_generate.text = self.Ayat()
            terjemahan = Tspan()
            font_size = self.options.font_size - 3
            terjemahan.style = f"font-family: serif; fill: #000000; font-size: {font_size};"
            terjemahan.text = f" \n{self.Terjemahan()}"
            quran_generate.append(terjemahan)
            
        return quran_generate

if __name__ == '__main__':
    quranID().run()
